package com.drohobytskyy.task02_OOP.View;

import com.drohobytskyy.task02_OOP.Model.Article;
import com.drohobytskyy.task02_OOP.Model.GroupOfArticles;

import java.util.ArrayList;

public interface View {
    void showArticles(ArrayList<Article> listOfArticles);
    void showGroups(ArrayList<GroupOfArticles> listOfGroups);
}