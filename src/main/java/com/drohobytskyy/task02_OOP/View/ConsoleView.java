package com.drohobytskyy.task02_OOP.View;

import com.drohobytskyy.task02_OOP.Model.Article;
import com.drohobytskyy.task02_OOP.Model.GroupOfArticles;

import java.util.ArrayList;

public class ConsoleView implements View {
    @Override
    public void showArticles(final ArrayList<Article> listOfArticles) {
        for (Article article: listOfArticles) {
            System.out.println(article);
        }
    }

    @Override
    public void showGroups(final ArrayList<GroupOfArticles> listOfGroups) {
        for (GroupOfArticles group: listOfGroups) {
            System.out.println(group);
        }
    }
}
