package com.drohobytskyy.task02_OOP.Model;

import java.util.ArrayList;

public interface ModelLayer {
    ArrayList<Article> getArticlesByName(String nameOfArticle);
    ArrayList<Article> getAllArticlesFromGroup(String nameOfGroup);
    ArrayList<Article> getAllArticles();
    ArrayList<GroupOfArticles> getAllGroups();

}
