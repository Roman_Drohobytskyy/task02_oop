package com.drohobytskyy.task02_OOP.Model;

import java.util.UUID;

public class GroupOfArticles {
    private static final int PERCENT_MAXIMUM = 100;
    private static final int FULL_PIECE = 1;
    private String id;
    private String name;
    private double extraPercent;
    private double extraMultiplier;

    private  void setId() {
        if (this.id == null) {
            this.id = UUID.randomUUID().toString().replace("-", "");
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public double getExtraPercent() {
        return extraPercent;
    }

    public void setExtraPercent(final int extraPercent) {
        this.extraPercent = extraPercent;
    }

    public double getExtraMultiplier() {
        return extraMultiplier;
    }

    private void setExtraMultiplier() {
        this.extraMultiplier = this.getExtraPercent() / PERCENT_MAXIMUM + FULL_PIECE;
    }

    public GroupOfArticles(final String name, final int extraPercent) {
        this.setId();
        this.setName(name);
        this.setExtraPercent(extraPercent);
        this.setExtraMultiplier();
    }

    @Override
    public String toString() {
        return "GroupOfArticles{"
                + "id='" + id + '\''
                + ", name='" + name + '\''
                + ", extraPercent=" + extraPercent
                + '}';
    }
}
