package com.drohobytskyy.task02_OOP.Model;

import java.util.ArrayList;

public class Model implements ModelLayer {
    private static ArrayList<GroupOfArticles> listOfGroups;
    private static ArrayList<Article> listOfArticles;
    private static ArrayList<Article> tempListOfArticles;

    public static void fillLists(){
        fillListOfGroups();
        fillListOfArticles();
    }

    private static void fillListOfGroups() {
        listOfGroups = new ArrayList<GroupOfArticles>();
        listOfGroups.add(new GroupOfArticles("Hair", 100));
        listOfGroups.add(new GroupOfArticles("Irons", 50));
        listOfGroups.add(new GroupOfArticles("Cosmetics", 150));
    }

    private static void fillListOfArticles() {
        listOfArticles = new ArrayList<Article>();

        listOfArticles.add(new Article("Hair 35-40cm", "Hair with keratin 35-40cm",
                0.60, listOfGroups.get(0)));
        listOfArticles.add(new Article("Hair 50-60cm", "Hair with keratin 50-60cm",
                0.80, listOfGroups.get(0)));
        listOfArticles.add(new Article("Hair 65-70cm", "Hair with keratin 65-70cm",
                1.60, listOfGroups.get(0)));

        listOfArticles.add(new Article("Pearl", "Hair straightener 2,5 cm width",
                90, listOfGroups.get(1)));
        listOfArticles.add(new Article("Glam", "Hair straightener 3 cm width",
                99, listOfGroups.get(1)));
        listOfArticles.add(new Article("White Devil", "Ultrasonic hair straightener",
                105, listOfGroups.get(1)));

        listOfArticles.add(new Article("Straight set", "Set of cosmetics for straight hair",
                40, listOfGroups.get(2)));
        listOfArticles.add(new Article("Curly set", "Set of cosmetics for curly hair",
                40, listOfGroups.get(2)));
        listOfArticles.add(new Article("Thermal protection", "Thermal protection spray for hair",
                12, listOfGroups.get(2)));

    }


    @Override
    public ArrayList<Article> getArticlesByName(final String nameOfArticle) {
        tempListOfArticles = new ArrayList<Article>();
        for (Article article: listOfArticles) {
            if (article.getName().toLowerCase().equals(nameOfArticle.toLowerCase())) {
                tempListOfArticles.add(article);
            }
        }
        return tempListOfArticles;
    }

    @Override
    public ArrayList<Article> getAllArticlesFromGroup(final String nameOfGroup) {
        tempListOfArticles = new ArrayList<Article>();
        for (Article article: listOfArticles) {
            if (article.getGroup().getName().toLowerCase().equals(nameOfGroup.toLowerCase())) {
                tempListOfArticles.add(article);
            }
        }
        return tempListOfArticles;
    }

    @Override
    public ArrayList<Article> getAllArticles() {
        return listOfArticles;
    }

    @Override
    public ArrayList<GroupOfArticles> getAllGroups() {
        return listOfGroups;
    }
}
