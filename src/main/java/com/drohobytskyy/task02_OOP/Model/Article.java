package com.drohobytskyy.task02_OOP.Model;

import java.util.UUID;

public class Article {
    private String id;
    private String name;
    private String description;
    private double cost;
    private double price;
    private GroupOfArticles group;

    public String getId() {
        return id;
    }

    public void setId() {
        if (this.id == null) {
            this.id = UUID.randomUUID().toString().replace("-", "");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(final double cost) {
        this.cost = cost;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public GroupOfArticles getGroup() {
        return group;
    }

    public void setGroup(final GroupOfArticles group) {
        this.group = group;
    }

    public Article(final String name, final String description, final double cost, final GroupOfArticles group) {
        this.setId();
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.group = group;
        this.price = this.getCost() * this.group.getExtraMultiplier();
    }

    @Override
    public String toString() {
        return "Article{"
                + "id='" + id + '\''
                + ", name='" + name + '\''
                + ", description='" + description + '\'' +
                ", cost=" + cost
                + ", price=" + price
                + ", group=" + group.getName()
                + '}';
    }
}
