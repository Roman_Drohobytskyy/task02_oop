package com.drohobytskyy.task02_OOP.Controller;


public class Main {

    private Main() {

    }

    public static void main(final String[] args) {
        Controller controller = new Controller();
        controller.execute();
    }
}
