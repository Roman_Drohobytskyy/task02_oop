package com.drohobytskyy.task02_OOP.Controller;

import com.drohobytskyy.task02_OOP.Model.GroupOfArticles;
import com.drohobytskyy.task02_OOP.Model.Model;
import com.drohobytskyy.task02_OOP.Model.ModelLayer;
import com.drohobytskyy.task02_OOP.View.ConsoleView;
import com.drohobytskyy.task02_OOP.View.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Controller {

    private ModelLayer modelLayer = new Model();
    private View view = new ConsoleView();
    private static BufferedReader bufferedReader;

    void execute() {
        Model.fillLists();
        System.out.println("Welcome to Homework task02_OOP!");
        run();
    }

    private void run() {
        try {
            runMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void runMenu() throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputText = "";

        for (;;) {
            System.out.println("--------------------------------------------");
            System.out.println("Please choose an appropriate item from menu:");
            System.out.println("1. Print all groups                         -> all groups");
            System.out.println("2. Print all existing articles              -> all articles");
            System.out.println("3. Print articles from a specific group     -> articles from group");
            System.out.println("4. Print an specific article by name        -> article by name");
            System.out.println("5. If you want to exit from the application -> exit");
            System.out.println("...........");

            try {
                inputText = bufferedReader.readLine().toLowerCase();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (inputText.equals("all groups")) {
                view.showGroups(modelLayer.getAllGroups());
            } else if (inputText.equals("all articles")) {
                view.showArticles(modelLayer.getAllArticles());
            } else if (inputText.equals("articles from group")) {
                view.showArticles(modelLayer.getAllArticlesFromGroup(inputCorrectGroupName()));
            } else if (inputText.equals("article by name")) {
                view.showArticles(modelLayer.getArticlesByName(inputArticlesName()));
            } else if (inputText.equals("exit")) {
                break;
            } else {
                System.out.println("Incorrect input! Please try again.");
            }
        }
        System.out.println("See you next time! Have a nice day!");
    }

    private String inputCorrectGroupName() throws IOException {
        String nameOfGroupFromBufferedReader = "";
        boolean isInputedNameCorrect = false;
        List<GroupOfArticles> groups = modelLayer.getAllGroups();
        String[] namesOfGroups = new String[groups.size()];

        for (int i = 0; i < groups.size(); i++) {
            namesOfGroups[i] = groups.get(i).getName();
            System.out.print(namesOfGroups[i] + ", ");
        }

        do {
            System.out.println("Please enter the name of the group or enter 'back' to go back:");
                nameOfGroupFromBufferedReader = bufferedReader.readLine().toLowerCase();
            for (GroupOfArticles group : groups) {
                if (nameOfGroupFromBufferedReader.equals("back")) {
                    return "";
                } else if (group.getName().toLowerCase().equals(nameOfGroupFromBufferedReader)) {
                    isInputedNameCorrect = !isInputedNameCorrect;
                    break;
                }
            }
            if (!isInputedNameCorrect) {
                System.out.println("Incorrect input! Please try again.");
            }
        } while (!isInputedNameCorrect);

        return nameOfGroupFromBufferedReader;
    }

    private String inputArticlesName() throws IOException {
        System.out.println("Please enter the name of the Article:");
        String nameOfArticleFromBufferedReader;
        nameOfArticleFromBufferedReader = bufferedReader.readLine().toLowerCase();
        return nameOfArticleFromBufferedReader;
    }



}
